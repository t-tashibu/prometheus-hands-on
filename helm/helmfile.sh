# helm-diffプラグインをインストール
helm plugin install https://github.com/databus23/helm-diff

# helmコマンドfileインストール
wget https://github.com/roboll/helmfile/releases/download/v0.144.0/helmfile_linux_amd64 
sudo mv helmfile_linux_amd64 /usr/local/bin/helmfile 
chmod +x /usr/local/bin/helmfile

# バージョン確認
helmfile version
